#!/bin/bash
# =======================
# Utilisation : chmod +x <$0> && ./<$0>
# Droits necessaires : sudoer
# Plateformes : Debian 7
# Objectifs : Installation d'une plateforme OpenERP 6.0.4 avec client web pour les besoins de formation des STMG
# Besoins : Un serveur fraîchement déployé, une configuration IP fixe, SELinux en mode permissif, un accès à un serveur de dépôts fonctionnel
# ================================

# ===========
# | Variables
# ===========
openerp_server_url_package="http://ent-cnfetp.eu/downloads/openerp/stmg/openerp-server-6.0.4.deb"
openerp_web_url_package="http://ent-cnfetp.eu/downloads/openerp/stmg/openerp-web-6.0.4.tar.gz"
openerp_web_url_init="http://ent-cnfetp.eu/downloads/openerp/stmg/openerp-web"
openerp_server_url_cfg="http://ent-cnfetp.eu/downloads/openerp/stmg/openerp-server.conf"
openerp_web_url_cfg="http://ent-cnfetp.eu/downloads/openerp/stmg/openerp-web.conf"
dependances="postgresql-client python-libxslt1 install python-cherrypy3 python-formencode python-pybabel python-simplejson python-pyparsing python python-psycopg2 python-reportlab python-egenix-mxdatetime python-tz python-pychart python-mako python-pydot python-lxml python-vobject python-yaml python-dateutil python-pychart python-webdav -y"
ip=`ip a | egrep 'inet.*eth0' | awk '{print $2}' | cut -f1 -d/`

# ==================
# | Message de début
# ==================
clear
echo "=============================================="
echo "--> Installation du serveur OpenERP 6.0.4     "
echo "=============================================="
sleep 3


# =============================
# | OS : Preparation du serveur
# =============================
chaine=$(cat /etc/debian_version)
if [[ $chaine = 7* ]]
	then
		wget -q --tries=10 --timeout=20 --spider http://google.com
		if [[ $? -eq 0 ]];
			then
				clear
				echo "--> + Preparation et mise à jour du serveur"
				sleep 2
				# Mise a jour des différents paquets existants
				sed -i "s/deb cdrom/# deb cdrom/g" /etc/apt/sources.list
				apt-get update
			
				#Si Update fonctionne pas stop
				if [[ $? -eq 0 ]]
					then 
						apt-get upgrade -y && apt-get install aptitude -y
						# ============================================
						# | PostgreSQL : Installation et configuration
						# ============================================
						clear
						echo "--> + Installation et configuration de PostgreSQL"
						sleep 2
						# Installation du SGBD PostgreSQL et de l'utilitaire d'administration
						aptitude install postgresql -y
						# Creation de la base pour OpenERP
						clear
						echo "--> Saisissez à la demande le mot de passe 'postgres'."
						su - postgres -c 'createuser -dEPRs openerp'
						# Ouverture des ports de PostgreSQL et ecoute sur le reseau
						sed -i "s/127.0.0.1\/32/0.0.0.0\/0/g" /etc/postgresql/9.1/main/pg_hba.conf
						sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/g" /etc/postgresql/9.1/main/postgresql.conf


						# =========================================
						# | OpenERP : Installation et configuration
						# =========================================
						clear
						echo "--> + Installation et configuration de OpenERP"
						sleep 2
						# Installation de toutes les dépendances nécessaires
						aptitude install $dependances
						# Installation du serveur et de sa config
						cd /tmp && wget "$openerp_server_url_package"
						dpkg -i openerp-server-6.0.4.deb
						rm openerp-server-6*.deb
						service openerp-server stop
						cd /etc && rm openerp-server.conf && wget "$openerp_server_url_cfg" && chown openerp: openerp-server.conf
						service openerp-server start
						# Decompression du client web + application des droits
						cd /tmp && wget "$openerp_web_url_package"
						mkdir /opt/openerp && chown openerp: /opt/openerp && cd /opt/openerp
						tar xf /tmp/openerp-web-6.0.4.tar.gz
						mv openerp-web-6.0.4 web
						chown -R openerp: web/

						# Creation du repertoire des logs
						mkdir /var/log/openerp
						chown openerp:root /var/log/openerp
						# Init du client web + conf
						cd /etc && wget "$openerp_web_url_cfg" && chown openerp: openerp-web.conf
						cd /etc/init.d && wget "$openerp_web_url_init"
						chmod 755 /etc/init.d/openerp-web
						chown root: /etc/init.d/openerp-web
						update-rc.d openerp-web defaults

						# ================
						# | Message de fin
						# ================
						service postgresql restart
						service openerp-server restart
						service openerp-web restart
						clear
						echo "=============================================="
						echo "--> + FIN de l'installation"
						echo " Adresse du client web : http://"$ip":8080"
						echo "=============================================="

					else 
						clear
						echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
						echo " ERREUR de mise à jour d'APT, merci de verifier vos sources de paquets"
						echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "

				fi

			else
				clear
				echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
				echo " ERREUR d'accès internet, veuillez verifier votre confirguration réseau"
				echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "

		fi

	else 
		clear
		echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
		echo " ERREUR : Le système d'exploitation nécessaire est Debian 7"
		echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
fi
