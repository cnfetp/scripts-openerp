#!/bin/bash
# =======================
# Utilisation : chmod +x <$0> && ./<$0>
# Droits necessaires : sudoer
# Plateformes : Debian 7
# Objectifs : Installation d'une plateforme OpenERP 6.1 avec client web pour les besoins de formation des Bac GA
# Besoins : Un serveur fraîchement déployé, une configuration IP fixe, SELinux en mode permissif, un accès à un serveur de dépôts fonctionnel
# ================================

# ===========
# | Variables
# ===========
openerp_server_url_package="http://ent-cnfetp.eu/downloads/openerp/ga/openerp_6.1-latest-1_all.deb"
dependances="python-dateutil python-feedparser python-gdata python-ldap python-libxslt1 python-lxml python-mako python-openid python-psycopg2 python-pybabel python-pychart python-pydot python-pyparsing python-reportlab python-simplejson python-tz python-vatnumber python-vobject python-webdav python-werkzeug python-xlwt python-yaml python-zsi -y"
ip=`ip a | egrep 'inet.*eth0' | awk '{print $2}' | cut -f1 -d/`
#================= 

#==================
# | Message de début
# ==================
clear
echo "=============================================="
echo "--> Installation du serveur OpenERP 6.1       "
echo "=============================================="
sleep 3

# =============================
# | OS : Preparation du serveur
# =============================

# Vérifie la version de la machine pour savoir si elle lance l'installation ou pas

chaine=$(cat /etc/debian_version)
if [[ $chaine = 7* ]]
	then 
		wget -q --tries=10 --timeout=20 --spider http://google.com
		if [[ $? -eq 0 ]];
			then
				clear
				echo "--> + Preparation et mise à jour du serveur"
				sleep 2
				# Mise a jour des différents paquets existants
				sed -i "s/deb cdrom/# deb cdrom/g" /etc/apt/sources.list
				apt-get update
			
				#Si Update fonctionne pas stop
				if [[ $? -eq 0 ]]
					then 

						apt-get upgrade -y && apt-get install aptitude -y
						# ============================================
						# | PostgreSQL : Installation et configuration
						# ============================================
						clear
						echo "--> + Installation et configuration de PostgreSQL"
						sleep 2
						# Installation du SGBD PostgreSQL et de l'utilitaire d'administration
						aptitude install postgresql -y
						# Creation de la base pour OpenERP
						clear
						echo "--> Saisissez à la demande le mot de passe 'postgres'."
						su - postgres -c 'createuser -dEPRs openerp'
						# Ouverture des ports de PostgreSQL et ecoute sur le reseau
						sed -i "s/127.0.0.1\/32/0.0.0.0\/0/g" /etc/postgresql/9.1/main/pg_hba.conf
						sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/g" /etc/postgresql/9.1/main/postgresql.conf


						# =========================================
						# | OpenERP : Installation et configuration
						# =========================================
						clear
						echo "--> + Installation et configuration de OpenERP"
						sleep 2
						# Installation de toutes les dépendances nécessaires
						aptitude install $dependances 
						# Dl de la version allinone de la 6.1
						wget "$openerp_server_url_package"
						dpkg -i openerp_6.1-latest-1_all.deb
						rm openerp_6.1*.deb


						# ================
						# | Message de fin
						# ================
						service postgresql restart
						service openerp restart
						clear
						echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
						echo "--> + FIN de l'installation"
						echo " Adresse du client web : http://"$ip":8069"
						echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"

					else 
						clear
						echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
						echo " ERREUR de mise à jour d'APT, merci de verifier vos sources de paquets"
						echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "

				fi

			else
				clear
				echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
				echo " ERREUR d'accès internet, veuillez verifier votre confirguration réseau"
				echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "

		fi

	else 
		clear
		echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
		echo " ERREUR : Le système d'exploitation nécessaire est Debian 7"
		echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
fi
