Ces scripts sont mis à disposition des établissements dans le but d'accompagner la réforme GA et STMG.

Ils permettent d'automatiser l'installation de plateformes OpenERP sur des distributions Debian.